<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use League\Csv\Reader;

class LoadProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $csv = Reader::createFromPath(storage_path('docs/product_groups.csv'));
        $records = $csv->getRecords();
        foreach ($records as $record) {
            $productCategory = new \App\ProductCategory();
            $productCategory->name = $record[0];
            $productCategory->save();
            $product = new \App\Product();
            $product->name = 'Place Holder';
            $productCategory->products()->save($product);

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
