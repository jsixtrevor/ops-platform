/*
|-------------------------------------------------------------------------------
| routes.js
|-------------------------------------------------------------------------------
| Contains all of the routes for the application
*/

/*
    Imports Vue and VueRouter to extend with the routes.
*/
import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from './components/Layout/Layout'
import store from './store'

import Home from './pages/Home'
import ClientForm from './pages/ClientForm'
import ClientTable from "./pages/ClientTable";
import EditClientProduct from "./pages/EditClientProduct";
/*
    Extends Vue to use Vue Router
*/
Vue.use(VueRouter)

function requireAuth(to, from, next) {
    /*
        Determines where we should send the user.
    */
    function proceed() {
        /*
            If the user has been loaded determine where we should
            send the user.
        */
        if (store.getters.getUserLoadStatus() == 2) {
            /*
                If the user is not empty, that means there's a user
                authenticated we allow them to continue. Otherwise, we
                send the user back to the home page.
            */
            if (store.getters.getUser != '') {
                switch (to.meta.permission) {
                    /*
                        If the route that requires authentication is a user, then we continue.
                        All users can access these routes
                    */
                    case 'user':
                        next();
                        break;
                    /*
                        If the route that requires authentication is an owner and the permission
                        the user has is greater than or equal to 1 (an owner or higher), we allow
                        access. Otherwise we redirect back to the cafes.
                    */
                    case 'owner':
                        if (store.getters.getUser.permission >= 1) {
                            next();
                        } else {
                            next('/cafes');
                        }
                        break;
                    /*
                        If the route that requires authentication is an admin and the permission
                        the user has is greater than or equal to 2 (an owner or higher), we allow
                        access. Otherwise we redirect back to the cafes.
                    */
                    case 'admin':
                        if (store.getters.getUser.permission >= 2) {
                            next();
                        } else {
                            next('/cafes');
                        }
                        break;
                    /*
                        If the route that requires authentication is a super admin and the permission
                        the user has is equal to 3 (a super admin), we allow
                        access. Otherwise we redirect back to the cafes.
                    */
                    case 'super-admin':
                        if (store.getters.getUser.permission == 3) {
                            next();
                        } else {
                            next('/cafes');
                        }
                        break;
                }
            } else {
                next('/cafes');
            }
        }
    }

    /*
        Confirms the user has been loaded
    */
    if (store.getters.getUserLoadStatus != 2) {
        /*
            If not, load the user
        */
        store.dispatch('loadUser');

        /*
            Watch for the user to be loaded. When it's finished, then
            we proceed.
        */
        store.watch(store.getters.getUserLoadStatus, function () {
            if (store.getters.getUserLoadStatus() == 2) {
                proceed();
            }
        });
    } else {
        /*
            User call completed, so we proceed
        */
        proceed()
    }
}

/*
	Makes a new VueRouter that we will use to run all of the routes
	for the app.
*/

export default new VueRouter({
    routes: [
        {
            path: '/',
            redirect: '/home',

        },
        {
            component: Layout,
            path: '/',
            children: [{
                path: '/home',
                component: Home,
                beforeEnter: requireAuth,
                meta: {
                    permission: 'user'
                }
            }, {
                path: '/client/create',
                component: ClientForm
            }, {
                path: '/clients',
                component: ClientTable
            }, {
                path: '/ClientProduct/:slug',
                component: EditClientProduct
            },
                {
                    path: 'client/:slug',
                    component: ClientForm
                }]
        }
    ]
})
