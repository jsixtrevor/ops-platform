<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {

    Route::get('/user', function (Request $request) {
        return \Auth::user();
    });

//    Route::patch('/updateUser', '');
    Route::post('/client', 'API\ClientController@store');
    Route::get('/clients', 'API\ClientController@index');
    Route::get('/client/{client}', 'API\ClientController@edit');
    Route::patch('/client/{client}', 'API\ClientController@update');
    Route::get('/ClientProduct/{clientProduct}', 'API\ClientProductController@edit');
    Route::patch('ClientProduct/{clientProduct}', 'API\ClientProductController@updateCustomFields');
});
