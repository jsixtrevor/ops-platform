<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $casts = ['keywords' => 'array', 'competitors' => 'array', 'geo_targeting' => 'array'];

    public function getCmsPasswordAttribute($value)
    {
        if (!empty($value)) {
            return decrypt($value);
        }
        return $value;
    }

    public function setCmsPasswordAttribute($value)
    {
        $this->attributes['cms_password'] = encrypt($value);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function client_products()
    {
        return $this->hasMany('App\ClientProduct');
    }

    public function custom_fields()
    {
        return $this->morphToMany('App\CustomField', 'customizable');

    }

    public function product_custom_fields()
    {
        return $this->hasManyThrough(
            'App\CustomField',
            'App\ClientProduct',
            'client_id',
            'custo'
        );
    }
}
