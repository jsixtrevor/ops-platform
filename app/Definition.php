<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Definition extends Model
{
    public function clients()
    {
        return $this->morphedByMany('App\Client', 'definable');
    }

    public function product_categories()
    {
        return $this->morphedByMany('App\ProductCategory', 'definable');
    }

    public function field_group()
    {
        return $this->belongsTo('App\FieldGroup');
    }
}
