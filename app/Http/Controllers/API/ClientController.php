<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
class ClientController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'contact_email' => 'required',
            'contact_name' => 'required',
            'contact_number' => 'required'
        ]);
        Client::create($request->all());
        return response(null, 200);
    }

    public function edit(Client $client)
    {
        return response()->json($client, 200);
    }

    public function update(Client $client, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'contact_email' => 'required',
            'contact_name' => 'required',
            'contact_number' => 'required'
        ]);
        $request = $request->toArray();
        $client->update($request);
        return response('', 200);
    }

    public function index()
    {
        $data = Client::all(['id', 'name']);
        return response()->json($data, 200);
    }
}
